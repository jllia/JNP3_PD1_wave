#include <windows.h>
#include <iostream>
#include <iomanip>
#include <numbers>
#include <cmath>
#include <d2d1.h>
#pragma comment(lib, "d2d1")

#include "basewin.h"

template <class T> void SafeRelease(T **ppT) {
    if (*ppT) {
        (*ppT)->Release();
        *ppT = NULL;
    }
}

class MainWindow : public BaseWindow<MainWindow> {
    ID2D1Factory            *pFactory;
    ID2D1HwndRenderTarget   *pRenderTarget;
    ID2D1SolidColorBrush    *pBrush;

    const INT POINTS_NUMBER = 51;
    FLOAT M_PI_4 = 3.14159265358979323846 / 4;
    std::pair<FLOAT, FLOAT> points[52][52];
    FLOAT calc[52][52];
    FLOAT x_val;
    FLOAT y_val;

    void    CalculateLayout();
    HRESULT CreateGraphicsResources();
    void    DiscardGraphicsResources();
    void    OnPaint(FLOAT angle);
    void    Resize();

public:

    MainWindow() : pFactory(NULL), pRenderTarget(NULL), pBrush(NULL) {}

    PCWSTR  ClassName() const { 
        return L"Wave Class"; 
    }
    LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
};

void MainWindow::CalculateLayout() {
    if (pRenderTarget != NULL) {
        D2D1_SIZE_F size = pRenderTarget->GetSize();
        x_val = size.width / 2;
        y_val = size.height / 2;
    }
}

HRESULT MainWindow::CreateGraphicsResources() {
    HRESULT hr = S_OK;
    if (pRenderTarget == NULL) {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

        hr = pFactory->CreateHwndRenderTarget(
            D2D1::RenderTargetProperties(),
            D2D1::HwndRenderTargetProperties(m_hwnd, size),
            &pRenderTarget);

        if (SUCCEEDED(hr)) {
            const D2D1_COLOR_F color = D2D1::ColorF(1.0f, 1.0f, 0);
            hr = pRenderTarget->CreateSolidColorBrush(color, &pBrush);

            if (SUCCEEDED(hr)) {
                CalculateLayout();
            }
        }
    }
    return hr;
}

void MainWindow::DiscardGraphicsResources() {
    SafeRelease(&pRenderTarget);
    SafeRelease(&pBrush);
}

void MainWindow::OnPaint(FLOAT angle) {

    HRESULT hr = CreateGraphicsResources();
    if (SUCCEEDED(hr)) {
        PAINTSTRUCT ps;
        BeginPaint(m_hwnd, &ps);
     
        pRenderTarget->BeginDraw();

        pRenderTarget->Clear(D2D1::ColorF(D2D1::ColorF::SkyBlue));

        RECT rc;
        GetClientRect(m_hwnd, &rc);

        FLOAT half_height = x_val;
        FLOAT half_width = y_val;

        FLOAT scale = 200.0f;
        FLOAT delta = 2.0f / POINTS_NUMBER;

        for (int i = 0; i < POINTS_NUMBER; i++) {
            for (int j = 0; j < POINTS_NUMBER; j++) {
                points[i][j].first = j * delta -1;
                points[i][j].second = i * delta -1;
                calc[i][j] = std::cos(10 * std::sqrt(points[i][j].first * points[i][j].first + points[i][j].second * points[i][j].second)) / 4;


                FLOAT new_pos = points[i][j].first * std::cos(M_PI_4) - points[i][j].second * std::sin(M_PI_4);
                points[i][j].second = points[i][j].first * std::sin(M_PI_4) + points[i][j].second * std::cos(M_PI_4);
                points[i][j].first = new_pos;

                new_pos = points[i][j].second * std::cos(angle) - calc[i][j] * std::sin(angle);
                calc[i][j] = points[i][j].second * std::sin(angle) + calc[i][j] * std::cos(angle);
                points[i][j].second = new_pos;


                points[i][j].first = (points[i][j].first * scale) + half_height;
                points[i][j].second = (points[i][j].second * scale) + half_width;
            }
        }

        for (int i = 0; i < POINTS_NUMBER; i++) {
            for (int j = 0; j < POINTS_NUMBER; j++) {
                if (i < POINTS_NUMBER - 1) {
                    pRenderTarget->DrawLine(D2D1::Point2F(points[i][j].first, points[i][j].second),
                        D2D1::Point2F(points[i + 1][j].first, points[i + 1][j].second), pBrush, 1.0f);
                }
                if (j < POINTS_NUMBER - 1) {
                    pRenderTarget->DrawLine(D2D1::Point2F(points[i][j].first, points[i][j].second),
                        D2D1::Point2F(points[i][j + 1].first, points[i][j + 1].second), pBrush, 1.0f);
                }
            }
        }


        hr = pRenderTarget->EndDraw();
        if (FAILED(hr) || hr == D2DERR_RECREATE_TARGET) {
            DiscardGraphicsResources();
        }
        EndPaint(m_hwnd, &ps);
    }
}

void MainWindow::Resize() {
    if (pRenderTarget != NULL) {
        RECT rc;
        GetClientRect(m_hwnd, &rc);

        D2D1_SIZE_U size = D2D1::SizeU(rc.right, rc.bottom);

        pRenderTarget->Resize(size);
        CalculateLayout();
        InvalidateRect(m_hwnd, NULL, FALSE);
    }
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR, int nCmdShow) {
    MainWindow win;

    if (!win.Create(L"Wave Project", WS_OVERLAPPEDWINDOW)) {
        return 0;
    }

    ShowWindow(win.Window(), nCmdShow);

    MSG msg = { };
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT MainWindow::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam) {
    static FLOAT rotation = 0.0f;
    switch (uMsg) {
        case WM_CREATE:
            SetTimer(m_hwnd, 1000, 10, nullptr);
            if (FAILED(D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &pFactory))) {
                return -1;
            }
            return 0;

        case WM_DESTROY:
            DiscardGraphicsResources(); 
            SafeRelease(&pFactory);
            KillTimer(m_hwnd, 1000);
            PostQuitMessage(0);
            return 0;

        case WM_PAINT:
            OnPaint(rotation);
            return 0;

        case WM_TIMER:
            rotation += 0.01f;
            InvalidateRect(m_hwnd, nullptr, true);
            return 0;

        case WM_SIZE:
            Resize();
            return 0;
    }
    return DefWindowProc(m_hwnd, uMsg, wParam, lParam);
}
